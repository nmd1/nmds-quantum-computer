INCLUDES=includes
OBJECTS= build
SOURCE=src
OUT=bin
NAME=qcomputer

DIR_CREATE = @mkdir -p 


CC=g++
CFLAGS=-I$(INCLUDES)
LIBS=


SRCS=$(wildcard $(SOURCE)/*.cpp)
HEAD=$(wildcard $(INCLUDES)/*.h)
OBJS=$(patsubst $(SOURCE)/%.cpp,$(OBJECTS)/%.o,$(SRCS))	

.PHONY: clean makesourcetree run


$(OBJECTS)/%.o: $(SOURCE)/%.cpp $(HEAD)
	@$(MAKE) -s makesourcetree
	@$(CC) -c $< $(CFLAGS) $(LIBS) -o $@



all: $(OBJS)
	@$(CC)  $^ -o $(OUT)/$(NAME)

loud: $(OBJS)
	$(CC)  $^ -o $(OUT)/$(NAME)

combine: test1 test2


run:
	@$(OUT)/$(NAME)

makesourcetree:
	@$(DIR_CREATE) $(OBJECTS) $(OUT)

clean:
	@rm -rf $(OBJECTS) $(OUT)
	@rm -f $(SOURCE)/*~  $(INCDIR)/*~ 

test1: $(OBJECTS)/matrix.o
	$(CC) -o $(OUT)/test1 $(OBJECTS)/matrix.o

test2: $(OBJECTS)/main.o
	$(CC) -o $(OUT)/test2 $(OBJECTS)/main.o
	