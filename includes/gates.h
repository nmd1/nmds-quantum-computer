#ifndef GATES_H
#define GATES_H

#include <complex>

#include "matrix.h"

using namespace std;


void init_std_gates();

complex<float> ** getX();

complex<float> ** getY();

complex<float> ** getZ();

complex<float> ** getCNOT();

complex<float> **getH();

#endif