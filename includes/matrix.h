#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <complex>

using namespace std;

int** makeMatrix(int);
complex<float> ** makeMatrix(short);
void printMatrix(int **, int);
void printMatrix(complex<float> **, int);
void zeroMatrix(int **, int);
void zeroMatrix(complex<float> **, int);
void deleteMatrix(int**, int);
int getDigits(int number);

void zeroState(complex<float> *, int);
complex<float> * makeState(int);
void printState(complex<float>* , int);


complex<float> * operate(complex<float> **, complex<float> *, int);

#endif