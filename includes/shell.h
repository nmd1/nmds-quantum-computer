#ifndef THE_SHELL_H
#define THE_SHELL_H

#include "matrix.h"
#include "gates.h"

#include <csignal>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <deque>
#include <vector>
#include <map>
#include <sstream> 
#include <complex>

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

using namespace std;

static volatile sig_atomic_t sigtally[32] = {};


void signalHandler(int);
void shell();
deque<string> parser(string);
void printStringVector(vector<string>);
void emptyQueue(deque<string>);

// Value-Defintions of the different String values
enum StandardCommands {  undef_cmd,
                         	 	help_cmd,
                          		echo_cmd,
                          		exit_cmd,
                          		matrix_cmd,
                          		qbit_cmd,
                          		complex_cmd,	
                          		run_cmd,
                          		get_cmd,
                          		act_cmd
                      };

enum StandardShellErrors {  	exit_err,
								not_enough_args_err,
								value_err,
                          };

void fill_cmds();
int processCommand(string, deque<string>);
// Map to associate the strings with the enum values
static std::map<std::string, StandardCommands> command_map;




int help_cmd_func();
int echo_cmd_func(string);
int exit_cmd_func();
int matrix_cmd_func(int);
int get_a_qubit();
int name_state_func(string);
int operate_func(string, string);

complex<float> complex_func();


// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}
complex<float> string_to_complex(string);

static map<string, complex<float> * > allstates;

#endif