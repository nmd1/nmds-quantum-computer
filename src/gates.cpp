#include "gates.h"


void init_std_gates() {
	//if(bits < 0) return	;
	short bits = 2;
	cout<<"Creating a matrix"<<endl;

	complex<float> ** X = getX();
	complex<float> ** Y = getY();	
	complex<float> ** Z = getZ();
	complex<float> ** CNOT  = getCNOT();

	printMatrix(X, 2);

	complex<float> * asdf = makeState(2);
	asdf[0] = 1;
	asdf[1] = 1i;
	//printState(asdf, 2);

	complex<float> * asdf2 = operate(X, asdf, 2);

	printState(asdf2, 2);


	cout<<endl;
	//zeroComplexMatrix(X,bits);
	cout<<"Done"<<endl;
}


complex<float> ** getX() {
	short bits = 2;
	complex<float> ** X = makeMatrix(bits);
	//[row][col]
	X[0][1] = 1;
	X[1][0] = 1;	
	return X;
}

complex<float> ** getY() {
	short bits = 2;
	complex<float> ** Y = makeMatrix(bits);
	Y[0][0] = 1i;
	Y[1][1] = 1i;	
	return Y;
}

complex<float> **getZ() {
	short bits = 2;
	complex<float> ** Z = makeMatrix(bits);
	Z[0][0] = 1;
	Z[1][1] = -1;	
	return Z;
}

complex<float> **getH() {
	short bits = 2;
	complex<float> ** H = makeMatrix(bits);
	H[0][0] = 1 / sqrt(2);
	H[0][1] = 1 / sqrt(2);
	H[1][0] = 1 / sqrt(2);	
	H[1][1] = -1 / sqrt(2);
	return H;
}


complex<float> ** getCNOT() {
	short bits = 4;
	complex<float> ** CNOT = makeMatrix(bits);
	CNOT[0][0] = 1;
	CNOT[1][1] = 1;	
	CNOT[2][3] = 1;
	CNOT[3][2] = 1;	
	//printMatrix(CNOT,4);
	return CNOT;
}

