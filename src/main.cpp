#include <iostream>
#include <string>
#include <csignal>
#include "matrix.h"
#include "qstrings.h"
#include "shell.h"
using namespace std;

/*
	Hey you decided to look at the source! Thanks.
	Writen in a caffinated haste by Nehemiah Dureus

*/



int main() { 
	signal(SIGINT, signalHandler);
	shell();
	//process("|x> = 453|hello world> + 1|oranges>");
	//process("|psi> = 1/sqrt(3)(3|0 1 0> + 2(|1 1 1> + sqrt(2)|999> + 1/2|1>) + .0|abc> - i/3|111>)");


	return 0;
}