#include "matrix.h"
using namespace std;

void zeroMatrix(int** matrix, int size) {
	for (int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			matrix[i][j] = 0;
		}
	}
}

void zeroMatrix(complex<float>** matrix, int size) {
	for (int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			matrix[i][j] = 0;
		}
	}
}

void zeroState(complex<float> * state, int size) {
	for (int i = 0; i < size; i++) {
		state[i] = 0;
	}
}
int** makeMatrix(int size) {

	int** a;
	a = new int*[size];
	for (int i = 0; i<size; i++) {
		a[i] = new int[size];
	}
	zeroMatrix(a,size);
	return a;
}

complex<float> ** makeMatrix(short size) {
	complex<float> ** a;
	a = new complex<float> *[size];
	for (int i = 0; i<size; i++) {
		a[i] = new complex<float>[size];
	}
	zeroMatrix(a,size);
	return a;
}

complex<float> * makeState(int size) {
	complex<float> * a;
	a = new complex<float> [size];
	zeroState(a, size);
	return a;
}

void printMatrix(int** matrix, int size) {
	int spacing = 3;
	for (int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			int value = *(*(matrix+i)+j);
			int digits = getDigits(value);
			cout << value;
			for (int k = 0; k<(spacing-digits); k++) {
				cout<<" ";
			}
		}
		cout << endl;
	}
}

void printMatrix(complex<float>** matrix, int size) {
	int spacing = 3;
	cout<<"Note a+bi --> (a,b)"<<endl;
	for (int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			complex<float> value = *(*(matrix+i)+j);
			// this comes out really well....Idk why, it shouldn't
			int digits = getDigits(abs(value));
			cout << value;
			for (int k = 0; k<(spacing-digits); k++) {
				cout<<" ";
			}
		}
		cout << endl;
	}
}

void printState(complex<float>* state, int size) {
	cout<<"Note a+bi --> (a,b)"<<endl;
	for (int i = 0; i < size; i++) {
		complex<float> value = *(state+i);
		cout<<value<<endl;
	}
}


void deleteMatrix(int** matrix, int size) {
	// need to use the delete[] operator because we used the new[] operator
	for(int i = 0; i < size; ++i){
		delete[] matrix[i];//deletes an inner array of integer;
	}

	delete[] matrix; //delete pointer holding array of pointers;
}

int getDigits(int number) {
	int digits=0;
	if(not number) return 1;
	while(number) {
		number/=10;
		digits++;
	}
	return digits;
}


complex<float> * operate(complex<float> ** U, complex<float> * psi, int size) {
	complex<float> * result = makeState(size);
	for(int i = 0; i < size; ++i) {
		for(int j = 0; j < size; ++j)
		{
		    result[i] += U[i][j] * psi[j];
		}
	}
	return result;
}