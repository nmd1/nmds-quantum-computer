#include "shell.h"



void signalHandler(int signum) {
	cout <<endl<< "Interrupt signal (" << signum << ") received.\n";

	// cleanup and close up stuff here  
	// terminate program  
	++sigtally[signum];
	switch (signum) {
		case SIGINT:
			cout<<"User interrupt, exiting...."<<endl;
			exit(signum);  	
		default:
			cout<<"Interesting Signal"<<endl;
	}


}

deque<string> parser(string command) {
	deque<string> output;
  	const char *  delimiter = " ,\t\n";

  	char * cmd = new char [command.length()+1];
  	std::strcpy (cmd, command.c_str());
	char * token = strtok (cmd,delimiter);
	while(token != NULL) {
		output.push_back(token);
		token = strtok (NULL,delimiter);
	}
	delete[] cmd;

	return output;
}


// Replace with generic version later on
void printStringVector(vector<string> a) {
	int j = 0;
	for (std::vector<string>::const_iterator i = a.begin(); i != a.end(); ++i) {
		std::cout << j <<":"<< *i << ' ';
		++j;
	}
}

void emptyQueue(deque<string> q) {
	int j = 0;
	while (!q.empty()) {
		std::cout << j <<":"<< q.front() << ' ';
		q.pop_front();
		j++;
	}
}

void fill_cmds() {
	command_map["help"] = help_cmd;
	command_map["echo"] = echo_cmd;
	command_map["exit"] = exit_cmd;
	command_map["matrix"] = matrix_cmd;
	command_map["qbit"] = qbit_cmd;
	command_map["complex"] = complex_cmd;	
	command_map["run"] = run_cmd;	
	command_map["show_state"] = get_cmd;	
	command_map["operate"] = act_cmd;		
	return;
}

void shell() {
	fill_cmds();
	bool kill = false;
	string command = "";
	while(!kill) {
		try {
			string line;
			cout << "qshell 0.9> ";
		  	getline(cin,line,'\n');
		  	if(trim(line).empty()) continue;
		  	deque<string> args = parser(line);
		  	command = args.front(); args.pop_front();
		  	processCommand(command, args);
		  	cout<<endl;
		} catch (StandardShellErrors e) {
			switch(e) {
				case exit_err:
					cout << "Goodbye!"<<endl;
					kill = true;
					break;
				case not_enough_args_err:
					cout <<command<<" could not execute: not enough arguments"<<endl;
					break;
				case value_err:
					cout <<command<<" could not execute: argument type is bad"<<endl;
					break;
				default:
					cout << "An exception occurred while processing" << command;
					cout << " ("<< e <<"). Exiting."<<endl;
					kill = true;
					break;
			}
		} catch (string s) {
			cout << "An exception has occured: "<<endl;
			cout << s;
			kill=true;
		} catch (...) {
			cout << "Something went very wrong."<<endl;
			throw;
		}
	}

}


int processCommand(string cmd, deque<string> args) {
	int status = 0;
	switch (command_map[cmd]) {
		case help_cmd:
			status = help_cmd_func();
			break;
		case echo_cmd:
			if (args.size() < 1) {
				throw not_enough_args_err;
			}
			status = echo_cmd_func(args[0]);
			break;
		case exit_cmd:
			status = exit_cmd_func();
			break;
		case matrix_cmd:
		{
			if (args.size() < 1) {
				throw not_enough_args_err;
			}
		    int input = 0; 
		    stringstream value(args[0]);
		    value >> input; 
			status = matrix_cmd_func(input);
			break;
		}
		case qbit_cmd:
		{
			status = get_a_qubit();
			break;
		}
		case complex_cmd: {
			complex_func();
			break;
		}
		case get_cmd: {
			if (args.size() < 1) {
				throw not_enough_args_err;
			}
			name_state_func(args[0]);
			break;
		}
		case act_cmd: {
			if (args.size() < 2) {
				throw not_enough_args_err;
			}
			//cout<<"Remember: Operator, State"<<endl;
			operate_func(args[0], args[1]);
			break;
		}
		default:
		{
			status = 1;
			cout<<"Command " << cmd << " not defined";
			break;
		}
	}
	return status;
}

int help_cmd_func() {
	map<std::string, StandardCommands>::iterator it;
	for ( it = command_map.begin(); it != command_map.end(); it++ )
	{
	    std::cout<< it->first;  // string (key)
	    cout<<endl;
	              //<< ':'
	              //<< it->second   // string's value 

	}
	return 0;
}

int echo_cmd_func(string value) {
	if(!value.empty())
		cout<<value;
	return 0;
}
int exit_cmd_func() {
	throw exit_err;
}
int matrix_cmd_func(int bits) {
	init_std_gates();
	/*
	if(bits < 0) throw value_err;
	cout<<"Creating a matrix"<<endl;
	int** first = makeMatrix(bits);
	cout<<"zeroing matrix"<<endl;
	zeroMatrix(first,bits);
	cout<<"printing matrix"<<endl;
	printMatrix(first,bits);
	//first[1][3] = 6;
	cout<<endl;
	printMatrix(first,bits);
	cout<<"Done"<<endl;
	*/
	return 0; 
}

int get_a_qubit() {
	string name;
	string zero;
	string one;
	cout << "Name: ";
	getline(cin,name,'\n');
	cout<<"|"<<name<<">";
	cout << "Define Qubit: ";
	cout << "0 Amplitude: ";
	getline(cin,zero,'\n');	
	cout << "1 Amplitude: ";
	getline(cin,one,'\n');	
	complex<float> * state =  makeState(2);
	state[0] = string_to_complex(zero);
	state[1] = string_to_complex(one);


	allstates[name] = state;
	//getline(cin,line,'\n');
	return 0;
}

complex<float> string_to_complex(string test) {
	size_t pos = 0;
	std::string token;
	string delimiter = "+";
	/*while ((pos = test.find(delimiter)) != std::string::npos) {
		token = test.substr(0, pos);
		std::cout << token << std::endl;
		test.erase(0, pos + delimiter.length());
	}*/
	std::string::iterator end_pos = std::remove(test.begin(), test.end(), ' ');
	test.erase(end_pos, test.end());
	std::string::iterator end_pos2 = std::remove(test.begin(), test.end(), 'i');
	test.erase(end_pos2, test.end());
  	std::replace(test.begin(), test.end(), '+', ',');
    std::istringstream is('(' + test + ')');
    std::complex<float> c;
    is >> c;
	return c;
}
complex<float> complex_func() {
	string test; 
	getline(cin, test, '\n');
	cout<<string_to_complex(test);
	return 0;
}


int name_state_func(string name) {
	complex<float> * temp = allstates[name];
	printState(temp, 2);
	return 0;
}


int operate_func(string op, string name) {
	complex<float> ** gate;
	if(op == "X") {
		gate = getX();
	} else if (op == "Y"){
		gate = getY();		
	} else if (op == "Z"){
		gate = getZ();	
	} else if (op == "H"){
		gate = getH();		
	} else if (op == "CNOT"){
		return 2; // will be implimented in the future	
	} else {
		return 1;
	}
	complex<float> * state = allstates[name];
	// Operates on state and returns result to state
	allstates[name] = operate(gate, state, 2);

	return 0;
}

